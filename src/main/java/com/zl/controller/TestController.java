package com.zl.controller;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.zl.model.Test;

public class TestController extends Controller{
	@Inject
	private Test testDao;
	public void index(){
		renderJson(this.testDao.find("select * from test"));
	}
}
