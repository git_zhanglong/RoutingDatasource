package com.zl.common.config;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class BusinessFlagInterceptor implements Interceptor{
	@Override
	public void intercept(Invocation inv) {
		String businessflag = (String)inv.getController().getRequest().getParameter("businessflag");
		PlatformContext.setBusinessFlag(businessflag);
		inv.invoke();
	}

}
