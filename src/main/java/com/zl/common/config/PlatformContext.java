package com.zl.common.config;

public class PlatformContext {
	private static ThreadLocal<String> businessFlag = new ThreadLocal<String>();
	public static String getBusinessFlag(){
		return businessFlag.get();
	}
	public static void setBusinessFlag(String businessflag){
		businessFlag.set(businessflag);
	}
	
}	
